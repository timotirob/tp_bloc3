<?php
  function isValid($date, $format = 'Y-m-d'){
    $dt = DateTime::createFromFormat($format, $date);


    return $dt && $dt->format($format) === $date;
  }

  var_dump(isValid('2021-09-12'));
  var_dump(isValid(''));
  var_dump(isValid('12-09-2021'));
  var_dump(isValid('12-09-2021','d-m-Y'));

?>