<!DOCTYPE html>
<html>
    <head>
        <title>Vérification date</title>
        <meta charset="utf-8">
    </head>
    <body>
    <form method="post">
        jour : <input type="text" name="jour" required minlength="1" maxlength="2" size="3"><br>
        mois : <input type="text" name="mois" required minlength="2" maxlength="2" size="3"><br>
        annee: <input type="text" name="annee" required minlength="1" maxlength="4" size="5"><br>
        <input type="submit" value="OK">
    </form>

        <?php
        function isValid($date, $format = 'd/m/Y'){
          $dt = DateTime::createFromFormat($format,$date);

          return $dt && $dt->format($format) === $date;
        }
        /*if (var_dump('12/09/2021')) {
            echo "La date est valide.";
        }      */

        var_dump(isValid);

        ?>
        
    </body>
</html>
